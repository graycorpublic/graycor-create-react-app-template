# cra-template-graycor

CRA template with: react router, msal, apollo-client, react-bootstrap, graphql, prop-types, mui.

## ⚗️ Technologies list

- [Apollo Client](https://www.apollographql.com/docs/react/)
- [React Router](https://www.reactrouter.com/)
- [React Bootstrap](https://react-bootstrap.github.io/)
- [MSAL-React](https://www.npmjs.com/package/@azure/msal-react)
- [Material UI](https://mui.com/)
- [Router](https://reactrouter.com/)
- [ESlint](https://eslint.org/) & [Prettier](https://prettier.io/)

<br />

# 🚀 Start using it

To use this template for your app you can run:

```sh
npx create-react-app my-app --template graycor
```

or

```sh
yarn create react-app my-app --template graycor
```

The `--template` parameter points to this template, note that `cra-template-` prefix is omitted.

<br />

# ⚠️ Warning

Cloning this repo pulls down the template only, not a bundled and configured Create React App.

<br />

# 🧬 Template structure

This is the structure of the files in the template:

```sh
    │
    ├── public                  # public files (favicon, .htaccess, manifest, ...)
    ├── src                     # source files
    │   ├── components
    │   ├── config              # Configuration Files (MSAL, GraphQL)
    │   ├── state               # Custom State
    │   ├── screens             # All the User Interface
    │   ├── utils               # Utilities
    │   ├── App.js
    │   ├── index.js
    │   ├── reportWebVitals.js
    │   └── setupTests.js
    ├── .eslintrc.js
    ├── .gitignore
    ├── .prettierrc.js
    ├── package.json
    └── README.md
```

<br />

# 📖 Learn More

This package includes scripts and configuration used by [Create React App](https://github.com/facebook/create-react-app).\
Please refer to its documentation:

- [Getting Started](https://facebook.github.io/create-react-app/docs/getting-started) – How to create a new app.
- [User Guide](https://facebook.github.io/create-react-app/) – How to develop apps bootstrapped with Create React App.

// ESLint configuration
// http://eslint.org/docs/user-guide/configuring
module.exports = {
  extends: ['react-app', 'prettier'],
  rules: {
    semi: 0,
    'no-console': 0,
  },
};

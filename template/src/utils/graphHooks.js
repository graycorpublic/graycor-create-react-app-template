/* eslint-disable react-hooks/rules-of-hooks */
import { useLazyQuery, useMutation } from '@apollo/client';
import queries from '../config/graphQueries';
import mutations from '../config/graphMutations';

const { getToDosQuery } = queries;
const { updateToDosMutation } = mutations;

export const getToDosHook = () => useLazyQuery(getToDosQuery);
export const updateToDosHook = () => useMutation(updateToDosMutation);

/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import { Routes, Route } from "react-router-dom";
import NoMatch from './screens/NoMatch';
import Home from './screens/Home';
import Layout from './screens/Layout';
import Wrapper from './screens/Wrapper';
import './App.css';

function App() {
  return (
    <div>
      <Wrapper>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<Home />} />
            <Route path="*" element={<NoMatch />} />
          </Route>
        </Routes>
      </Wrapper>
    </div>
  );
}

export default App;

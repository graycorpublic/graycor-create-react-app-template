import React from 'react';
import ContentWrapper from '../../components/ContentWrapper';

import './index.css';

const HomeScreen = () => {
  return (
    <ContentWrapper pageName="Home" hideButtons>
        <div className="container-lg homeContainer">
          <div className="row">
            Click on the Navigation on the Left
          </div>
        </div>
    </ContentWrapper>
  )
};

export default HomeScreen;

import React, { useState, useEffect } from 'react';
import { useMsal } from "@azure/msal-react";
import { Link, useLocation } from "react-router-dom";
import Header from '../../components/Header';

const Wrapper = (props) => {
  const [pagePath, setPagePath] = useState('/');
  const [showMenu, setShowMenu] = useState(false);
  const { instance } = useMsal();
  let location = useLocation();

  const activeAccount = instance.getActiveAccount();
  useEffect(() => {
      if (activeAccount) {
          setShowMenu(true);
      } else {
          setShowMenu(false);
      }
  }, [activeAccount]);

  useEffect(() => {
    if (location) {
      const { pathname } = location;
      if (pathname) {
        setPagePath(pathname);
      }
    }
  }, [location]);

  const checkIfPath = (pathname) => (pathname === pagePath) ? ' active' : '';

  return (<div>
    <Header />
    <div className="container-fluid">
      <div className="row">
        <nav id="sidebarMenu" className="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
          <div className="position-sticky pt-3">
            {showMenu && (<>
              <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                <span>Graycor Template</span>
              </h6>
              <ul className="nav flex-column">
                <li className="nav-item">
                  <Link className={`nav-link ${checkIfPath('/')}`} aria-current="page" to="/">
                    <ion-icon name="home-outline" style={{ paddingRight: 5 }} />
                    Home
                  </Link>
                </li>
              </ul>
            </>)}
          </div>
        </nav>
        {props.children}
      </div>
    </div>
  </div>)
};

export default Wrapper;

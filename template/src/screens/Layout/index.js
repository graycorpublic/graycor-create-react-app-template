/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import { Outlet } from "react-router-dom";
import { useAccount, useMsal, AuthenticatedTemplate, UnauthenticatedTemplate, withMsal } from "@azure/msal-react";
import ContentWrapper from '../../components/ContentWrapper';

const Layout = (props) => {
  const { instance, accounts } = useMsal();
  const account = useAccount(accounts[0] || {});
  const [apiData, setApiData] = useState(null);
  console.info('apiData:', apiData);

  useEffect(() => {
    if (account) {
        instance.acquireTokenSilent({
            scopes: ["User.Read"],
            account: account
        }).then((response) => {
            if(response) {
                setApiData(response);
            }
        });
    }
  }, [account, instance]);

  return (
    <React.Fragment>
      <AuthenticatedTemplate>
        {<Outlet />}
      </AuthenticatedTemplate>
      <UnauthenticatedTemplate>
        <ContentWrapper hideButtons pageName="Not Logged In...">
        </ContentWrapper>
      </UnauthenticatedTemplate>
    </React.Fragment>
  );
};

export default withMsal(Layout);
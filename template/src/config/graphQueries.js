import { gql } from "@apollo/client";

const getToDos = gql`
query getToDos($input: ToDosInputType!) {
  getToDos(input: $input) {
    id
  }
}`;

const queries = {
  getToDosQuery: getToDos,
};

export default queries;

// Azure AD Configuration
// Create App Registration store in .env
// REACT_APP_AUTHORITY=https://login.microsoftonline.com/000aaa00-1234-1234-1234-AAACCCDDDEEE
// REACT_APP_CLIENT_ID=123abc00-1234-1234-1234-123AAACCCDDD
// REACT_APP_REDIRECT_URL=/

export const msalConfig = {
  auth: {
    authority:
      process.env.REACT_APP_AUTHORITY,
    clientId: process.env.REACT_APP_CLIENT_ID,
    redirectUri: process.env.REACT_APP_REDIRECT_URL,
    postLogoutRedirectUri: process.env.REACT_APP_REDIRECT_URL,
    // After being redirected to the "redirectUri" page, should user
    // be redirected back to the Url where their login originated from?
    navigateToLoginRequestUrl: false,
  },
  cache: {
    cacheLocation: 'localStorage',
    storeAuthStateInCookie: true,
  },
};

// SET THE SCOPES
export const loginRequest = {
  scopes: ["User.Read", "User.ReadBasic.All", "People.Read"],
};

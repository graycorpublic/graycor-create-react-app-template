import { gql } from "@apollo/client";

const updateToDos = gql`
  mutation updateToDos($input: ToDoInputType!) {
    updateToDos(input: $input) {
      id
    }
  }
`;

const mutations = {
  updateToDosMutation: updateToDos,
};

export default mutations;
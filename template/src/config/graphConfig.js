// SET YOUR GRAPH URL IN ENV

import {
  ApolloClient,
  InMemoryCache,
  HttpLink,
} from "@apollo/client";
import { setContext } from '@apollo/client/link/context';

const graphqlURI = new HttpLink({ uri: process.env.REACT_APP_GRAPH_URL });

const headerLink = setContext((_, { headers }) => ({
  headers: {
    ...headers,
    localauthorization: true,
  },
}));

const graphQLLink = headerLink.concat(graphqlURI);

export const apolloClient = new ApolloClient({
  link: graphQLLink,
  cache: new InMemoryCache()
});
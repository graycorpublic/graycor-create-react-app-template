import { Link } from "react-router-dom";
import WelcomeName from './WelcomeName';
import SignInSignOutButton from './SignInSignOutButton';

const Header = () => (
  <header className="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
    <div className="navbar-brand col-md-3 col-lg-2 me-0 px-3" style={{ display: 'flex', justifyContent: 'center' }}>
      <Link to="/">
        <img src="GCRLogo2.png" alt="Graycor" width="125" />
      </Link>
    </div>
    <div style={{ color: 'white', paddingRight: 10 }}><WelcomeName /> | <SignInSignOutButton /></div>
    <button className="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
  </header>
);

export default Header;
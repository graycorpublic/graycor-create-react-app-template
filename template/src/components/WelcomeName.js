import { useEffect, useState } from "react";
import { useMsal } from "@azure/msal-react";

const WelcomeName = () => {
    const { instance } = useMsal();
    const [name, setName] = useState(null);

    const activeAccount = instance.getActiveAccount();
    console.info('Active Account:', activeAccount);
    useEffect(() => {
        if (activeAccount) {
            setName(activeAccount.name);
        } else {
            setName(null);
        }
    }, [activeAccount]);

    if (name) {
        return <>Welcome, {name}</>;
    } else {
        return null;
    }
};

export default WelcomeName;
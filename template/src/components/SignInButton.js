import { useMsal } from "@azure/msal-react";
import Button from "@mui/material/Button";
import { loginRequest } from "../config/authConfig";

const SignInButton = () => {
    const { instance } = useMsal();
    return (
      <Button variant="text" onClick={() => instance.loginRedirect(loginRequest)} color="inherit">
        Login
      </Button>
    );
};

export default SignInButton;

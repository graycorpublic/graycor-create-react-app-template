import PropTypes from 'prop-types';

const ContentWrapper = ({ children, pageName }) => (
  <main className="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <h1 className="h2">{pageName}</h1>
      <div className="btn-toolbar mb-2 mb-md-0">
        HI
      </div>
    </div>
    {children}
  </main>
);

ContentWrapper.propTypes = {
  buttonDisabled: PropTypes.bool,
  buttonColor: PropTypes.string,
  children: PropTypes.any,
  pageName: PropTypes.string,
  hideButtons: PropTypes.bool,
  buttonFunction: PropTypes.func,
  buttonText: PropTypes.string,
}

ContentWrapper.defaultProps = {
  buttonDisabled: false,
  buttonColor: 'green',
  children: undefined,
  pageName: '',
  hideButtons: true,
  buttonFunction: () => {},
  buttonText: 'Save',
}

export default ContentWrapper;
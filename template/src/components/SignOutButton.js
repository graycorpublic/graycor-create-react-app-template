import { useMsal } from "@azure/msal-react";
import Button from "@mui/material/Button";

const SignInButton = () => {
    const { instance } = useMsal();
    return (
      <Button variant="text" onClick={() => instance.logoutRedirect()} color="inherit">
        Logout
      </Button>
    );
};

export default SignInButton;
